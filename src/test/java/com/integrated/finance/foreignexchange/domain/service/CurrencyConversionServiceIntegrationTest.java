package com.integrated.finance.foreignexchange.domain.service;

import com.integrated.finance.foreignexchange.base.controller.response.DataResponse;
import com.integrated.finance.foreignexchange.domain.base.BaseSpringTest;
import com.integrated.finance.foreignexchange.domain.configuration.FixerCurrencyServiceTestConfiguration;
import com.integrated.finance.foreignexchange.domain.controller.response.CurrencyConversionResponse;
import com.integrated.finance.foreignexchange.domain.controller.response.GetExchangeRateResponse;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ContextConfiguration(classes = {FixerCurrencyServiceTestConfiguration.class})
public class CurrencyConversionServiceIntegrationTest extends BaseSpringTest {

    private static final Integer SCALE_TWO = 2;
    private static final String ACCESS_KEY = "3f4370476adcfb40eced240a13ec65d2";

    @Autowired
    CurrencyConversionService currencyConversionService;

    @Test
    public void should_convert_currency_when_given_EUR_TO_TRY(){
        BigDecimal ONE_EUR_EQUAL_TRY = new BigDecimal("12.464716");
        DataResponse<CurrencyConversionResponse> currencyConversionResponse =
                currencyConversionService.convertCurrency(ACCESS_KEY, Currency.EUR, Currency.TRY, 100);

        assertThat(currencyConversionResponse.isSuccess(),is(true));
        assertThat(currencyConversionResponse.getData().getTargetCurrencyAmount(),
                is(ONE_EUR_EQUAL_TRY.multiply(new BigDecimal("100"))));
    }

    @Test
    public void should_convert_currency_when_given_EUR_to_USD(){
        BigDecimal ONE_EUR_EQUAL_USD = new BigDecimal("1.134042");
        DataResponse<CurrencyConversionResponse> currencyConversionResponse =
                currencyConversionService.convertCurrency(ACCESS_KEY, Currency.EUR, Currency.USD, 100);

        assertThat(currencyConversionResponse.isSuccess(),is(true));
        assertThat(currencyConversionResponse.getData().getTargetCurrencyAmount(),
                is(ONE_EUR_EQUAL_USD.multiply(new BigDecimal("100"))));
    }

    @Test
    public void should_convert_currency_when_given_TRY_TO_EUR(){
        BigDecimal ONE_EUR_EQUAL_TRY = new BigDecimal("12.464716");
        DataResponse<CurrencyConversionResponse> currencyConversionResponse =
                currencyConversionService.convertCurrency(ACCESS_KEY, Currency.TRY, Currency.EUR, 100);

        assertThat(currencyConversionResponse.isSuccess(),is(true));
        assertThat(currencyConversionResponse.getData().getTargetCurrencyAmount(),
                is(new BigDecimal("100").divide(ONE_EUR_EQUAL_TRY, SCALE_TWO, RoundingMode.HALF_DOWN)));
    }

    @Test
    public void should_convert_currency_when_given_USD_TO_EUR(){
        BigDecimal ONE_EUR_EQUAL_USD = new BigDecimal("1.134042");
        DataResponse<CurrencyConversionResponse> currencyConversionResponse =
                currencyConversionService.convertCurrency(ACCESS_KEY, Currency.EUR, Currency.TRY, 100);

        assertThat(currencyConversionResponse.isSuccess(),is(true));
        assertThat(currencyConversionResponse.getData().getTargetCurrencyAmount(),
                is(ONE_EUR_EQUAL_USD.multiply(new BigDecimal("100"))));
    }
}
