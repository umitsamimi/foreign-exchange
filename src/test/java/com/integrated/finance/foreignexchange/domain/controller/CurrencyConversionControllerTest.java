package com.integrated.finance.foreignexchange.domain.controller;

import com.integrated.finance.foreignexchange.base.controller.response.DataResponse;
import com.integrated.finance.foreignexchange.base.controller.response.helper.ResponseHelper;
import com.integrated.finance.foreignexchange.domain.controller.response.CurrencyConversionResponse;
import com.integrated.finance.foreignexchange.domain.controller.response.GetExchangeRateResponse;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;
import com.integrated.finance.foreignexchange.domain.service.CurrencyConversionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CurrencyConversionControllerTest {

    private final String ENDPOINT = "/api/currency/convert";

    @Mock
    CurrencyConversionService currencyConversionService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(new CurrencyConversionController(currencyConversionService)).build();
    }

    @Test
    public void when_convert_currency_service_call_then_return_target_currency_amount_success() throws Exception {
        String ACCESS_KEY = "3f4370476adcfb40eced240a13ec65d2";

        CurrencyConversionResponse currencyConversionResponse = CurrencyConversionResponse
                .builder()
                .targetCurrencyAmount(new BigDecimal("8.1"))
                .transactionId(1l)
                .build();
        DataResponse<CurrencyConversionResponse> response = ResponseHelper.of(currencyConversionResponse);

        when(currencyConversionService.convertCurrency(any(),any(),any(),any())).thenReturn(response);

        mockMvc.perform(get(ENDPOINT)
                .param("accessKey", ACCESS_KEY)
                .param("from", Currency.EUR.name())
                .param("to", Currency.USD.name())
                .param("amount", "10")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(response.isSuccess()))
                .andExpect(jsonPath("$.data.targetCurrencyAmount")
                        .value(response.getData().getTargetCurrencyAmount()))
                .andExpect(jsonPath("$.data.transactionId")
                        .value(response.getData().getTransactionId()));
    }
}
