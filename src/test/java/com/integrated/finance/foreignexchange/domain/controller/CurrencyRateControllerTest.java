package com.integrated.finance.foreignexchange.domain.controller;

import com.integrated.finance.foreignexchange.base.controller.response.DataResponse;
import com.integrated.finance.foreignexchange.base.controller.response.helper.ResponseHelper;
import com.integrated.finance.foreignexchange.domain.controller.response.GetExchangeRateResponse;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;
import com.integrated.finance.foreignexchange.domain.service.CurrencyRateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CurrencyRateControllerTest {

    private final String ENDPOINT = "/api/exchange/{from}/{to}";

    @Mock
    CurrencyRateService currencyRateService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(new CurrencyRateController(currencyRateService)).build();
    }

    @Test
    public void when_currency_exchange_service_call_then_return_exchange_rate_success() throws Exception {
        String ACCESS_KEY = "3f4370476adcfb40eced240a13ec65d2";

        GetExchangeRateResponse getExchangeRateResponse = GetExchangeRateResponse.builder()
                .exchangeRate(new BigDecimal("1.134042"))
                .build();
        DataResponse<GetExchangeRateResponse> response = ResponseHelper.of(getExchangeRateResponse);

        when(currencyRateService.getExchangeRate(ACCESS_KEY,Currency.USD,Currency.EUR)).thenReturn(response);

        mockMvc.perform(get(ENDPOINT, Currency.USD.name(),Currency.EUR.name())
                .param("accessKey", ACCESS_KEY)
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(response.isSuccess()))
                .andExpect(jsonPath("$.data.exchangeRate").value(response.getData().getExchangeRate()));
    }
}
