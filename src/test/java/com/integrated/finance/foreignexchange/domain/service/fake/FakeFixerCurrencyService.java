package com.integrated.finance.foreignexchange.domain.service.fake;

import com.integrated.finance.foreignexchange.delegate.fixer.service.FixerCurrencyService;
import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;
import com.integrated.finance.foreignexchange.delegate.model.dto.Rate;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FakeFixerCurrencyService implements FixerCurrencyService {

    private static final List<Rate> rates = new ArrayList<>();
    {
        rates.add(createRate(Currency.TRY.name(), new BigDecimal("12.464716")));
        rates.add(createRate(Currency.USD.name(), new BigDecimal("1.134042")));
        rates.add(createRate(Currency.EUR.name(), BigDecimal.ONE));
        rates.add(createRate(Currency.JPY.name(), new BigDecimal("129.74183")));
    }

    @Override
    public CurrencyExchangeRate getCurrencyRates(String accessKey) {
        return CurrencyExchangeRate.builder()
                .rates(rates)
                .source(Currency.EUR.name())
                .success(true)
                .build();
    }

    private Rate createRate(final String name, final BigDecimal rate) {
        return Rate.builder()
                .rate(rate)
                .name(name)
                .build();
    }
}
