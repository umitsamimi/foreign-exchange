package com.integrated.finance.foreignexchange.domain.service;

import com.integrated.finance.foreignexchange.base.controller.response.DataResponse;
import com.integrated.finance.foreignexchange.domain.base.BaseSpringTest;
import com.integrated.finance.foreignexchange.domain.configuration.FixerCurrencyServiceTestConfiguration;
import com.integrated.finance.foreignexchange.domain.controller.response.GetExchangeRateResponse;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ContextConfiguration(classes = {FixerCurrencyServiceTestConfiguration.class})
public class CurrencyRateServiceIntegrationTest extends BaseSpringTest {

    private static final Integer SCALE_TWO = 2;
    private static final String ACCESS_KEY = "3f4370476adcfb40eced240a13ec65d2";
    @Autowired
    private CurrencyRateService currencyRateService;

    @Test
    public void should_return_rate_when_given_TRY_and_EUR(){
        BigDecimal EUR_TRY_RATE = new BigDecimal("12.464716");
        DataResponse<GetExchangeRateResponse> exchangeRate = currencyRateService
                .getExchangeRate(ACCESS_KEY, Currency.TRY, Currency.EUR);

        assertThat(exchangeRate.isSuccess(),is(true));
        assertThat(exchangeRate.getData().getExchangeRate(),
                is(BigDecimal.ONE.divide(EUR_TRY_RATE,SCALE_TWO, RoundingMode.HALF_DOWN)));
    }

    @Test
    public void should_return_rate_when_given_EUR_and_TRY(){
        BigDecimal EUR_TRY_RATE = new BigDecimal("12.464716");
        DataResponse<GetExchangeRateResponse> exchangeRate = currencyRateService
                .getExchangeRate(ACCESS_KEY, Currency.EUR, Currency.TRY);

        assertThat(exchangeRate.isSuccess(),is(true));
        assertThat(exchangeRate.getData().getExchangeRate(),
                is(EUR_TRY_RATE));
    }

    @Test
    public void should_return_rate_when_given_EUR_and_USD(){
       BigDecimal EUR_USD_RATE = new BigDecimal("1.134042");
        DataResponse<GetExchangeRateResponse> exchangeRate = currencyRateService
                .getExchangeRate(ACCESS_KEY, Currency.EUR, Currency.USD);

        assertThat(exchangeRate.isSuccess(),is(true));
        assertThat(exchangeRate.getData().getExchangeRate(),
                is(EUR_USD_RATE));
    }

    @Test
    public void should_return_rate_when_given_USD_and_EUR(){
        BigDecimal EUR_USD_RATE = new BigDecimal("1.134042");
        DataResponse<GetExchangeRateResponse> exchangeRate = currencyRateService
                .getExchangeRate(ACCESS_KEY, Currency.USD, Currency.EUR);

        assertThat(exchangeRate.isSuccess(),is(true));
        assertThat(exchangeRate.getData().getExchangeRate(),
                is(BigDecimal.ONE.divide(EUR_USD_RATE,SCALE_TWO, RoundingMode.HALF_DOWN)));
    }
}