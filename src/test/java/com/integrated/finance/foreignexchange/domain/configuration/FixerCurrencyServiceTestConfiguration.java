package com.integrated.finance.foreignexchange.domain.configuration;

import com.integrated.finance.foreignexchange.delegate.fixer.service.FixerCurrencyService;
import com.integrated.finance.foreignexchange.domain.service.fake.FakeFixerCurrencyService;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

@TestConfiguration
public class FixerCurrencyServiceTestConfiguration {

    @Bean
    @Primary
    public FixerCurrencyService fakeFixerCurrencyService(){
        return new FakeFixerCurrencyService();
    }
}
