package com.integrated.finance.foreignexchange.base.controller.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public abstract class ApiResponse {

    private boolean success;
    private BusinessError error;
}