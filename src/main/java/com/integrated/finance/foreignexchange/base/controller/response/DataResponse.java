package com.integrated.finance.foreignexchange.base.controller.response;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class DataResponse<R> extends ApiResponse {

    private R data;
}