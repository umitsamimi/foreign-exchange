package com.integrated.finance.foreignexchange.base.component;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
public class ErrorMessageHelper {

    @Qualifier("errorMessageProperties")
    private final Properties errorMessageProperties;

    public ErrorMessageHelper(Properties errorMessageProperties) {
        this.errorMessageProperties = errorMessageProperties;
    }

    public String getErrorMessage(String error) {
        error = error + ".message";
        return getError(error);
    }

    public String getErrorCode(String error) {
        error = error + ".code";
        return getError(error);
    }

    private String getError(String errorKey) {
        String error = null;
        if (errorMessageProperties.containsKey(errorKey)) {
            error = errorMessageProperties.getProperty(errorKey);
        }
        return error;
    }
}