package com.integrated.finance.foreignexchange.base.controller.response.helper;

import com.integrated.finance.foreignexchange.base.controller.response.DataResponse;

public class ResponseHelper {

    private ResponseHelper() {
    }

    public static <R> DataResponse<R> of(R data) {
        return DataResponse.<R>builder()
                .data(data)
                .success(true)
                .build();
    }
    /* TODO
    public static VoidResponse of() {
        return VoidResponse.builder()
                .success(true)
                .build();
    }*/


}