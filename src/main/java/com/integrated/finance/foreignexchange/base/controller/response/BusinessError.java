package com.integrated.finance.foreignexchange.base.controller.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BusinessError {

    private String code;
    private String message;
}