package com.integrated.finance.foreignexchange.base.controller.advice;

import com.integrated.finance.foreignexchange.base.component.ErrorMessageHelper;
import com.integrated.finance.foreignexchange.base.controller.response.BusinessError;
import com.integrated.finance.foreignexchange.base.controller.response.VoidResponse;
import com.integrated.finance.foreignexchange.base.exception.ApiException;
import com.integrated.finance.foreignexchange.delegate.fixer.exception.FixerCurrencyServiceConnectionFailedException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RequiredArgsConstructor
@RestControllerAdvice
public class ControllerErrorHandlerAdvice {

    private static final String GENERAL_ERROR = "general.base.error";

    private final ErrorMessageHelper errorMessageHelper;

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public VoidResponse handle(Exception exception) {
        log.error("ERROR : ", exception);
        String errorMessage = errorMessageHelper.getErrorMessage(GENERAL_ERROR);
        String errorCode = errorMessageHelper.getErrorCode(GENERAL_ERROR);

        BusinessError businessError = BusinessError.builder()
                .code(errorCode)
                .message(errorMessage)
                .build();

        return VoidResponse.builder()
                .error(businessError)
                .success(false)
                .build();
    }

    @Order(Ordered.HIGHEST_PRECEDENCE)
    @ExceptionHandler(FixerCurrencyServiceConnectionFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public VoidResponse handle(FixerCurrencyServiceConnectionFailedException exception) {
        String errorMessage = errorMessageHelper.getErrorMessage(GENERAL_ERROR);
        String errorCode = errorMessageHelper.getErrorCode(GENERAL_ERROR);

        BusinessError businessError = BusinessError.builder()
                .code(errorCode)
                .message(errorMessage)
                .build();

        return VoidResponse.builder()
                .error(businessError)
                .success(false)
                .build();
    }

    @Order(Ordered.HIGHEST_PRECEDENCE)
    @ExceptionHandler(ApiException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public VoidResponse handle(ApiException exception) {
        String errorMessage = errorMessageHelper.getErrorMessage(exception.getMessage());
        String errorCode = errorMessageHelper.getErrorCode(exception.getMessage());

        BusinessError businessError = BusinessError.builder()
                .code(errorCode)
                .message(errorMessage)
                .build();

        return VoidResponse.builder()
                .error(businessError)
                .success(false)
                .build();
    }
}