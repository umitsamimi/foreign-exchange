package com.integrated.finance.foreignexchange.delegate.fixer.exception;

public class FixerCurrencyServiceConnectionFailedException extends RuntimeException {

    public FixerCurrencyServiceConnectionFailedException() {
    super();
}

    public FixerCurrencyServiceConnectionFailedException(String message) {
        super(message);
    }

    public FixerCurrencyServiceConnectionFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public FixerCurrencyServiceConnectionFailedException(Throwable cause) {
        super(cause);
    }

    protected FixerCurrencyServiceConnectionFailedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
