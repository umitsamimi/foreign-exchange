package com.integrated.finance.foreignexchange.delegate.apilayer.response;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Map;

@Getter
@Setter
public class ApiLayerCurrencyResponse {
    private boolean success;
    private String source;
    private Map<String, BigDecimal> quotes;
}
