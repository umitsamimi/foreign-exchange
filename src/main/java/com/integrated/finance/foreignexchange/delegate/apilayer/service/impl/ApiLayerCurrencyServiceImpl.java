package com.integrated.finance.foreignexchange.delegate.apilayer.service.impl;

import com.integrated.finance.foreignexchange.base.exception.ApiException;
import com.integrated.finance.foreignexchange.delegate.model.converter.ApiLayerCurrencyResponseConverter;
import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;
import com.integrated.finance.foreignexchange.delegate.apilayer.response.ApiLayerCurrencyResponse;
import com.integrated.finance.foreignexchange.delegate.apilayer.service.ApiLayerCurrencyService;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
class ApiLayerCurrencyServiceImpl implements ApiLayerCurrencyService {

    public CurrencyExchangeRate convertCurrency() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ApiLayerCurrencyResponse> apiLayerCurrencyResponseEntity = restTemplate
                .exchange(null, HttpMethod.GET, null, ApiLayerCurrencyResponse.class);

        if(apiLayerCurrencyResponseEntity.getStatusCode().equals(HttpStatus.OK)){
            return ApiLayerCurrencyResponseConverter.convert(apiLayerCurrencyResponseEntity.getBody());
        }
        throw new ApiException("delegate.apilayer.service.connection.failed");
    }
}
