package com.integrated.finance.foreignexchange.delegate.apilayer.service;

import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;

public interface ApiLayerCurrencyService {
    CurrencyExchangeRate convertCurrency();
}
