package com.integrated.finance.foreignexchange.delegate.fixer.service.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FixerSerivceConnectionInfo {
    @Value("${fixer.currency.service.url}")
    private String url;
    @Value("${fixer.currency.service.path}")
    private String path;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
