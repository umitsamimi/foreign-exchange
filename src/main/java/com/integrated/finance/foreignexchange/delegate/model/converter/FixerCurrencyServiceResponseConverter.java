package com.integrated.finance.foreignexchange.delegate.model.converter;

import com.integrated.finance.foreignexchange.delegate.fixer.response.FixerCurrencyServiceResponse;
import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;
import com.integrated.finance.foreignexchange.delegate.model.dto.Rate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class FixerCurrencyServiceResponseConverter {
    private FixerCurrencyServiceResponseConverter(){}

    public static CurrencyExchangeRate convert(FixerCurrencyServiceResponse response) {
        return CurrencyExchangeRate.builder()
                .source(response.getBase())
                .success(response.isSuccess())
                .rates(convert(response.getRates()))
                .build();
    }
    private static List<Rate> convert(Map<String, BigDecimal> rates){
        return rates
                .entrySet()
                .stream()
                .map(entry -> Rate.builder().name(entry.getKey()).rate(entry.getValue()).build())
                .collect(Collectors.toList());
    }
}
