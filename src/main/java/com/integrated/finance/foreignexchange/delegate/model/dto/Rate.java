package com.integrated.finance.foreignexchange.delegate.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class Rate {
    private String name;
    private BigDecimal rate;
}
