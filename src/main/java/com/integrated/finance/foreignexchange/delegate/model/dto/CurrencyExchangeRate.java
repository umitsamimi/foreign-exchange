package com.integrated.finance.foreignexchange.delegate.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class CurrencyExchangeRate {
    private boolean success;
    private String source;
    private List<Rate> rates;
}
