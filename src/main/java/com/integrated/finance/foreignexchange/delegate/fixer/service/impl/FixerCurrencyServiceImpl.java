package com.integrated.finance.foreignexchange.delegate.fixer.service.impl;

import com.integrated.finance.foreignexchange.delegate.fixer.exception.FixerCurrencyServiceConnectionFailedException;
import com.integrated.finance.foreignexchange.delegate.fixer.response.FixerCurrencyServiceResponse;
import com.integrated.finance.foreignexchange.delegate.fixer.service.FixerCurrencyService;
import com.integrated.finance.foreignexchange.delegate.fixer.service.component.FixerSerivceConnectionInfo;
import com.integrated.finance.foreignexchange.delegate.model.converter.FixerCurrencyServiceResponseConverter;
import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
@RequiredArgsConstructor
class FixerCurrencyServiceImpl implements FixerCurrencyService {

    private static final String CURRENCY_RATE_SERVICE = "currencyRateService";
    private final FixerSerivceConnectionInfo fixerSerivceConnectionInfo;

    @CircuitBreaker(name=CURRENCY_RATE_SERVICE, fallbackMethod = "getCurrencyRatesFallBack")
    public CurrencyExchangeRate getCurrencyRates(final String accessKey) {
        RestTemplate restTemplate = new RestTemplate();
        final String path = fixerSerivceConnectionInfo.getPath() + "?access_key="+ accessKey + "&format=1";
        ResponseEntity<FixerCurrencyServiceResponse> fixerCurrencyServiceResponse = restTemplate
                .exchange(fixerSerivceConnectionInfo.getUrl()+path,
                        HttpMethod.GET, null, FixerCurrencyServiceResponse.class);

        if(fixerCurrencyServiceResponse.getStatusCode().equals(HttpStatus.OK)){
            return FixerCurrencyServiceResponseConverter.convert(fixerCurrencyServiceResponse.getBody());
        }
        throw new FixerCurrencyServiceConnectionFailedException("delegate.fixer.service.connection.failed");
    }

    public CurrencyExchangeRate getCurrencyRatesFallBack(Exception e) {
        return CurrencyExchangeRate.builder()
                .rates(Collections.emptyList())
                .source(Currency.EUR.name())
                .build();
    }
}
