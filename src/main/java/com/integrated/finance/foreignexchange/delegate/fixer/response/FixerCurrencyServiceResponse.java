package com.integrated.finance.foreignexchange.delegate.fixer.response;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Map;

@Getter
@Setter
public class FixerCurrencyServiceResponse {
    private boolean success;
    private String base;
    private Map<String, BigDecimal> rates;
}