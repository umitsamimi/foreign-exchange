package com.integrated.finance.foreignexchange.delegate.model.converter;

import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;
import com.integrated.finance.foreignexchange.delegate.model.dto.Rate;
import com.integrated.finance.foreignexchange.delegate.apilayer.response.ApiLayerCurrencyResponse;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class ApiLayerCurrencyResponseConverter {
    private ApiLayerCurrencyResponseConverter(){}
    public static CurrencyExchangeRate convert(ApiLayerCurrencyResponse response) {
        return CurrencyExchangeRate.builder()
                .source(response.getSource())
                .success(response.isSuccess())
                .rates(convert(response.getQuotes()))
                .build();
    }
    private static List<Rate> convert(Map<String, BigDecimal> quotes){
        return quotes
                .entrySet()
                .stream()
                .map(entry -> Rate.builder().name(entry.getKey()).rate(entry.getValue()).build())
                .collect(Collectors.toList());
    }
}
