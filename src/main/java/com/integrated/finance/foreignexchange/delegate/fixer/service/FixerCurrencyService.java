package com.integrated.finance.foreignexchange.delegate.fixer.service;

import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;

public interface FixerCurrencyService {
    CurrencyExchangeRate getCurrencyRates(final String accessKey);
}
