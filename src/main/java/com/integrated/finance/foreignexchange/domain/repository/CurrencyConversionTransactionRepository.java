package com.integrated.finance.foreignexchange.domain.repository;

import com.integrated.finance.foreignexchange.domain.model.entity.CurrencyConversionTransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyConversionTransactionRepository
        extends JpaRepository<CurrencyConversionTransactionEntity, Long> {
}
