package com.integrated.finance.foreignexchange.domain.ruleengine.currencyconversion;

import com.integrated.finance.foreignexchange.base.function.TetraFunction;
import com.integrated.finance.foreignexchange.base.predicate.TriPredicate;
import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;

import java.util.HashMap;
import java.util.Map;

public final class CurrencyConversionRuleMatrix {
    private CurrencyConversionRuleMatrix(){}

    private static Map<TriPredicate, TetraFunction> ruleMatrix = new HashMap<>();

    static{
        ruleMatrix.put(CurrencyConversionPredicate.WHEN_FROM_CURRENCY_EQUAL_BASE, CurrencyConversionFunction.MULTIPLY_AMOUNT_WITH_RATE);
        ruleMatrix.put(CurrencyConversionPredicate.WHEN_TO_CURRENCY_EQUAL_BASE, CurrencyConversionFunction.DIVIDE_AMOUNT_BY_RATE);
    }

    public static TetraFunction getRule(CurrencyExchangeRate currencyExchangeRate, String from, String to){
        return ruleMatrix
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey().test(currencyExchangeRate,from,to))
                .map(entry -> entry.getValue())
                .findFirst()
                .orElse(CurrencyConversionFunction.CONVERT_CURRENCY_THEN_MULTIPLY_WITH_AMOUT); //DEFAULT
    }
}
