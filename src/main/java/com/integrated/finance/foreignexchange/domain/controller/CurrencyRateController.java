package com.integrated.finance.foreignexchange.domain.controller;

import com.integrated.finance.foreignexchange.base.controller.response.DataResponse;
import com.integrated.finance.foreignexchange.domain.controller.response.GetExchangeRateResponse;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;
import com.integrated.finance.foreignexchange.domain.service.CurrencyRateService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/exchange")
@RequiredArgsConstructor
public class CurrencyRateController {

    private final CurrencyRateService currencyRateService;

    @GetMapping("/{from}/{to}")
    public DataResponse<GetExchangeRateResponse> getExchangeRate(@RequestParam String accessKey,
                                                                 @PathVariable("from") Currency from,
                                                                 @PathVariable("to") Currency to) {
        return currencyRateService.getExchangeRate(accessKey,from,to);
    }
}
