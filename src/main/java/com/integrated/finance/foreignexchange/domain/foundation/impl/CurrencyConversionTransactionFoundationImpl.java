package com.integrated.finance.foreignexchange.domain.foundation.impl;

import com.integrated.finance.foreignexchange.domain.converter.CurrencyConversionTransactionConverter;
import com.integrated.finance.foreignexchange.domain.foundation.CurrencyConversionTransactionFoundation;
import com.integrated.finance.foreignexchange.domain.model.dto.CurrencyConversionTransactionDto;
import com.integrated.finance.foreignexchange.domain.model.entity.CurrencyConversionTransactionEntity;
import com.integrated.finance.foreignexchange.domain.repository.CurrencyConversionTransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class CurrencyConversionTransactionFoundationImpl implements CurrencyConversionTransactionFoundation {

    private final CurrencyConversionTransactionRepository currencyConversionTransactionRepository;
    public CurrencyConversionTransactionDto save(CurrencyConversionTransactionDto currencyConversionTransaction){
        CurrencyConversionTransactionEntity entity = CurrencyConversionTransactionConverter.convert(currencyConversionTransaction);
        currencyConversionTransactionRepository.save(entity);
        return CurrencyConversionTransactionConverter.convert(entity);
    }
}
