package com.integrated.finance.foreignexchange.domain.service;

import com.integrated.finance.foreignexchange.base.controller.response.DataResponse;
import com.integrated.finance.foreignexchange.domain.controller.response.GetExchangeRateResponse;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;

public interface CurrencyRateService {
    DataResponse<GetExchangeRateResponse> getExchangeRate(String accessKey, Currency from, Currency to);
}
