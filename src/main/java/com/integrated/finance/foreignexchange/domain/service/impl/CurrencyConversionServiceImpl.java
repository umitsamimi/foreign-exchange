package com.integrated.finance.foreignexchange.domain.service.impl;

import com.integrated.finance.foreignexchange.base.controller.response.DataResponse;
import com.integrated.finance.foreignexchange.base.controller.response.helper.ResponseHelper;
import com.integrated.finance.foreignexchange.delegate.fixer.service.FixerCurrencyService;
import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;
import com.integrated.finance.foreignexchange.domain.controller.response.CurrencyConversionResponse;
import com.integrated.finance.foreignexchange.domain.foundation.CurrencyConversionTransactionFoundation;
import com.integrated.finance.foreignexchange.domain.model.dto.CurrencyConversionTransactionDto;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;
import com.integrated.finance.foreignexchange.domain.ruleengine.currencyconversion.CurrencyConversionRuleMatrix;
import com.integrated.finance.foreignexchange.domain.service.CurrencyConversionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
class CurrencyConversionServiceImpl implements CurrencyConversionService {

    private final FixerCurrencyService fixerCurrencyService;
    private final CurrencyConversionTransactionFoundation currencyConversionTransactionFoundation;

    @Override
    public DataResponse<CurrencyConversionResponse> convertCurrency(String accessKey, Currency from, Currency to, Integer amount) {
        final CurrencyExchangeRate currencyExchangeRate = fixerCurrencyService.getCurrencyRates(accessKey);

        BigDecimal result = (BigDecimal) CurrencyConversionRuleMatrix.getRule(currencyExchangeRate, from.name(), to.name())
                .apply(currencyExchangeRate, from.name(), to.name(), amount);

        CurrencyConversionTransactionDto currencyConversionTransaction = currencyConversionTransactionFoundation.
                save(getCurrencyConversionTransaction(accessKey, from, to, result));

        return ResponseHelper.of(CurrencyConversionResponse
                .builder()
                .targetCurrencyAmount(result)
                .transactionId(currencyConversionTransaction.getId())
                .build());
    }

    private CurrencyConversionTransactionDto getCurrencyConversionTransaction(String accessKey, Currency from, Currency to, BigDecimal result) {
        return CurrencyConversionTransactionDto.builder()
                .toCurrency(from)
                .fromCurrency(to)
                .targetCurrencyAmount(result)
                .accessKey(accessKey)
                .build();
    }
}
