package com.integrated.finance.foreignexchange.domain.converter;

import com.integrated.finance.foreignexchange.domain.model.dto.CurrencyConversionTransactionDto;
import com.integrated.finance.foreignexchange.domain.model.entity.CurrencyConversionTransactionEntity;

public final class CurrencyConversionTransactionConverter {
    private CurrencyConversionTransactionConverter(){}

    public static CurrencyConversionTransactionEntity convert(CurrencyConversionTransactionDto currencyConversionTransaction){
        return CurrencyConversionTransactionEntity.builder()
                .targetCurrencyAmount(currencyConversionTransaction.getTargetCurrencyAmount())
                .fromCurrency(currencyConversionTransaction.getFromCurrency())
                .toCurrency(currencyConversionTransaction.getToCurrency())
                .accessKey(currencyConversionTransaction.getAccessKey())
                .build();
    }

    public static CurrencyConversionTransactionDto convert(CurrencyConversionTransactionEntity entity){
        return CurrencyConversionTransactionDto.builder()
                .toCurrency(entity.getToCurrency())
                .fromCurrency(entity.getFromCurrency())
                .accessKey(entity.getAccessKey())
                .targetCurrencyAmount(entity.getTargetCurrencyAmount())
                .id(entity.getId())
                .build();
    }
}
