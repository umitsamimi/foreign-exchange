package com.integrated.finance.foreignexchange.domain.controller.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class CurrencyConversionResponse {
   private BigDecimal targetCurrencyAmount;
   private Long transactionId;
}
