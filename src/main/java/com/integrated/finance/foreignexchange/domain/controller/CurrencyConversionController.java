package com.integrated.finance.foreignexchange.domain.controller;

import com.integrated.finance.foreignexchange.base.controller.response.DataResponse;
import com.integrated.finance.foreignexchange.domain.controller.response.CurrencyConversionResponse;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;
import com.integrated.finance.foreignexchange.domain.service.CurrencyConversionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/currency/convert")
@RequiredArgsConstructor
public class CurrencyConversionController {

    private final CurrencyConversionService currencyConversionService;

    @GetMapping
    public DataResponse<CurrencyConversionResponse> convertCurrency(@RequestParam String accessKey,
                                                                    @RequestParam Currency from,
                                                                    @RequestParam Currency to,
                                                                    @RequestParam Integer amount) {
        return currencyConversionService.convertCurrency(accessKey,from,to,amount);
    }
}
