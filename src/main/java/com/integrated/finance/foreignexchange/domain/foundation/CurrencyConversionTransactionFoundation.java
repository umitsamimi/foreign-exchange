package com.integrated.finance.foreignexchange.domain.foundation;

import com.integrated.finance.foreignexchange.domain.model.dto.CurrencyConversionTransactionDto;

public interface CurrencyConversionTransactionFoundation {
    CurrencyConversionTransactionDto save(CurrencyConversionTransactionDto currencyConversionTransaction);
}
