package com.integrated.finance.foreignexchange.domain.model.entity;

import com.integrated.finance.foreignexchange.base.model.entity.BaseEntity;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "currency_conversion_transaction")
public class CurrencyConversionTransactionEntity extends BaseEntity {

    @Column(name = "access_key")
    private String accessKey;

    @Column(name = "from_currency", nullable = false)
    @Enumerated(EnumType.STRING)
    private Currency fromCurrency;

    @Column(name = "to_currency", nullable = false)
    @Enumerated(EnumType.STRING)
    private Currency toCurrency;

    @Column(name = "target_currency_amount")
    private BigDecimal targetCurrencyAmount;
}
