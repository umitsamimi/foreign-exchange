package com.integrated.finance.foreignexchange.domain.service.impl;

import com.integrated.finance.foreignexchange.base.controller.response.DataResponse;
import com.integrated.finance.foreignexchange.base.controller.response.helper.ResponseHelper;
import com.integrated.finance.foreignexchange.delegate.fixer.service.FixerCurrencyService;
import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;
import com.integrated.finance.foreignexchange.domain.controller.response.GetExchangeRateResponse;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;
import com.integrated.finance.foreignexchange.domain.ruleengine.currencyrate.CurrencyRateRuleMatrix;
import com.integrated.finance.foreignexchange.domain.service.CurrencyRateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
class CurrencyRateServiceImpl implements CurrencyRateService {
    private final FixerCurrencyService fixerCurrencyService;
    @Override
    public DataResponse<GetExchangeRateResponse> getExchangeRate
            (final String accessKey, final Currency from, final Currency to) {
        final CurrencyExchangeRate currencyExchangeRate = fixerCurrencyService.getCurrencyRates(accessKey);

        final BigDecimal result = (BigDecimal) CurrencyRateRuleMatrix
                .getRule(currencyExchangeRate, from.name(), to.name())
                .apply(currencyExchangeRate, from.name(), to.name());

        return ResponseHelper.of(GetExchangeRateResponse
                .builder()
                .exchangeRate(result)
                .build());
    }
}
