package com.integrated.finance.foreignexchange.domain.controller.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class GetExchangeRateResponse {
    private BigDecimal exchangeRate;
}
