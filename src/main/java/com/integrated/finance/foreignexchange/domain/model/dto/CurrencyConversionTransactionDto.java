package com.integrated.finance.foreignexchange.domain.model.dto;

import com.integrated.finance.foreignexchange.domain.model.enums.Currency;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class CurrencyConversionTransactionDto {

    private Long id;
    private String accessKey;
    private Currency fromCurrency;
    private Currency toCurrency;
    private BigDecimal targetCurrencyAmount;
}
