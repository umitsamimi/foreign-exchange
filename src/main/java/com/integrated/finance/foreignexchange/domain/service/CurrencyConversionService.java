package com.integrated.finance.foreignexchange.domain.service;

import com.integrated.finance.foreignexchange.base.controller.response.DataResponse;
import com.integrated.finance.foreignexchange.domain.controller.response.CurrencyConversionResponse;
import com.integrated.finance.foreignexchange.domain.model.enums.Currency;

public interface CurrencyConversionService {
    DataResponse<CurrencyConversionResponse> convertCurrency(String accessKey, Currency from, Currency to, Integer amount);
}
