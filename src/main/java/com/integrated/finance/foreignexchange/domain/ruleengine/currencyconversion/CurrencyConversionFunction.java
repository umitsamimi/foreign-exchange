package com.integrated.finance.foreignexchange.domain.ruleengine.currencyconversion;

import com.integrated.finance.foreignexchange.base.exception.ApiException;
import com.integrated.finance.foreignexchange.base.function.TetraFunction;
import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;
import com.integrated.finance.foreignexchange.delegate.model.dto.Rate;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class CurrencyConversionFunction {
    private CurrencyConversionFunction(){}
    private static final Integer SCALE_TWO = 2;
    public static final TetraFunction<CurrencyExchangeRate, String, String, Integer, BigDecimal> MULTIPLY_AMOUNT_WITH_RATE
            = (currencyExchangeRate, from, to, amount) -> {
        Rate rate = currencyExchangeRate.getRates()
                .stream().filter(item -> item.getName().equals(to))
                .findFirst().orElseThrow(() -> new ApiException(""));//TODO
        return rate.getRate().multiply(new BigDecimal(amount));
    };

    public static final TetraFunction<CurrencyExchangeRate, String, String, Integer, BigDecimal> DIVIDE_AMOUNT_BY_RATE
            = (currencyExchangeRate, from, to, amount) -> {
        Rate rateFrom = currencyExchangeRate.getRates()
                .stream()
                .filter(rate -> rate.getName().equals(from))
                .findFirst()
                .orElseThrow(() -> new ApiException("")); //TODO

        return new BigDecimal(amount).divide(rateFrom.getRate(), SCALE_TWO, RoundingMode.HALF_DOWN);
    };

    public static final TetraFunction<CurrencyExchangeRate, String, String, Integer, BigDecimal>
            CONVERT_CURRENCY_THEN_MULTIPLY_WITH_AMOUT = (currencyExchangeRate, from, to, amount) -> {
        Rate rateTo = currencyExchangeRate.getRates()
                .stream()
                .filter(rate -> rate.getName().equals(to))
                .findFirst()
                .orElseThrow(() -> new ApiException("")); //TODO

        Rate rateFrom = currencyExchangeRate.getRates()
                .stream()
                .filter(rate -> rate.getName().equals(from))
                .findFirst()
                .orElseThrow(() -> new ApiException("")); //TODO

        return rateTo.getRate().divide(rateFrom.getRate(), SCALE_TWO, RoundingMode.HALF_DOWN).multiply(new BigDecimal(amount));
    };
}
