package com.integrated.finance.foreignexchange.domain.ruleengine.currencyrate;

import com.integrated.finance.foreignexchange.base.exception.ApiException;
import com.integrated.finance.foreignexchange.base.function.TriFunction;
import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;
import com.integrated.finance.foreignexchange.delegate.model.dto.Rate;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class CurrencyRateFunction {
    private CurrencyRateFunction(){}
    private static final Integer SCALE_TWO = 2;
    public static final TriFunction<CurrencyExchangeRate, String, String, BigDecimal> RETURN_TO_CURRENCY_RATE
            = (currencyExchangeRate, from, to) -> {
        Rate rate = currencyExchangeRate.getRates()
                .stream().filter(item -> item.getName().equals(to))
                .findFirst().orElseThrow(() -> new ApiException(""));//TODO
        return rate.getRate();
    };

    public static final TriFunction<CurrencyExchangeRate, String, String, BigDecimal> RETURN_TO_CURRENCY_DIVIDED_BY_ONE
            = (currencyExchangeRate, from, to) -> {
        Rate fromRate = currencyExchangeRate.getRates()
                .stream().filter(item -> item.getName().equals(from))
                .findFirst().orElseThrow(() -> new ApiException(""));//TODO
        return BigDecimal.ONE.divide(fromRate.getRate(), SCALE_TWO, RoundingMode.HALF_DOWN);
    };

    public static final TriFunction<CurrencyExchangeRate, String, String, BigDecimal> RETURN_RATE_DIVIDE_TO_AND_FROM_CURRENCY_RATE
            = (currencyExchangeRate, from, to) -> {
        Rate rateFrom = currencyExchangeRate.getRates()
                .stream().filter(item -> item.getName().equals(from))
                .findFirst().orElseThrow(() -> new ApiException(""));//TODO

        Rate rateTo = currencyExchangeRate.getRates()
                .stream().filter(item -> item.getName().equals(to))
                .findFirst().orElseThrow(() -> new ApiException(""));//TODO
        return rateTo.getRate().divide(rateFrom.getRate(), SCALE_TWO, RoundingMode.HALF_DOWN);
    };
}
