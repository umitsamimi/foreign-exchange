package com.integrated.finance.foreignexchange.domain.ruleengine.currencyrate;

import com.integrated.finance.foreignexchange.base.function.TriFunction;
import com.integrated.finance.foreignexchange.base.predicate.TriPredicate;
import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;

import java.util.HashMap;
import java.util.Map;

public final class CurrencyRateRuleMatrix {
    private CurrencyRateRuleMatrix(){}

    private static Map<TriPredicate, TriFunction> ruleMatrix = new HashMap<>();

    static{
        ruleMatrix.put(CurrencyRatePredicate.WHEN_FROM_CURRENCY_EQUAL_BASE, CurrencyRateFunction.RETURN_TO_CURRENCY_RATE);
        ruleMatrix.put(CurrencyRatePredicate.WHEN_TO_CURRENCY_EQUAL_BASE, CurrencyRateFunction.RETURN_TO_CURRENCY_DIVIDED_BY_ONE);
    }

    public static TriFunction getRule(CurrencyExchangeRate currencyExchangeRate, String from, String to){
        return ruleMatrix
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey().test(currencyExchangeRate,from,to))
                .map(entry -> entry.getValue())
                .findFirst()
                .orElse(CurrencyRateFunction.RETURN_RATE_DIVIDE_TO_AND_FROM_CURRENCY_RATE); //DEFAULT
    }
}
