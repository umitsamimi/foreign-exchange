package com.integrated.finance.foreignexchange.domain.ruleengine.currencyconversion;

import com.integrated.finance.foreignexchange.base.predicate.TriPredicate;
import com.integrated.finance.foreignexchange.delegate.model.dto.CurrencyExchangeRate;

public final class CurrencyConversionPredicate {
    private CurrencyConversionPredicate(){}
    public static final TriPredicate<CurrencyExchangeRate, String, String> WHEN_FROM_CURRENCY_EQUAL_BASE =
            (currencyExchangeRate, from, to) -> currencyExchangeRate.getSource().equals(from);

    public static final TriPredicate<CurrencyExchangeRate, String, String> WHEN_TO_CURRENCY_EQUAL_BASE =
            (currencyExchangeRate, from, to) -> currencyExchangeRate.getSource().equals(to);

    public static final TriPredicate<CurrencyExchangeRate, String, String> WHEN_FROM_AND_TO_CURRENCY_NOT_EQUAL_BASE =
            (currencyExchangeRate, from, to) -> !currencyExchangeRate.getSource().equals(from);
}
